<header>
    <div class="top-menu">
        <div class="container">
            <div class="row">
                <div class="col-md a">
                    <div class="golden">
                        <a href="index.php">
                            <img src="images/logo/logo-2.jpg" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md b">
                    <div class="list">
                        <ul>
                            <li class="f"><a href="#">قوانین و مقررات</a></li>
                            <li><a href="#">فروش اقساطی </a></li>
                            <li><a href="#">همه کالاها</a></li>
                            <li><a href="#">تخفیف دارها</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span style="font-size:18px" class="color-gold flaticon flaticon-list-with-dots"></span>
                                دسته بندی محصولات
                                <span class="color-gold flaticon flaticon-down-arrow"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="tab">
                                    <button class="tablinks" onmouseover="openCity(event, 'hosein1')"><span class="flaticon flaticon-next"></span> product</button>
                                    <button class="tablinks" onmouseover="openCity(event, 'hosein2')"><span class="flaticon flaticon-next"></span> product</button>
                                    <button class="tablinks" onmouseover="openCity(event, 'hosein3')"><span class="flaticon flaticon-next"></span> product</button>
                                    <button class="tablinks" onmouseover="openCity(event, 'hosein4')"><span class="flaticon flaticon-next"></span> product</button>
                                    <button class="tablinks" onmouseover="openCity(event, 'hosein5')"><span class="flaticon flaticon-next"></span> product</button>
                                </div>

                                <div id="hosein1" class="tabcontent">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="">test</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="">test</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/product/cache_cats_1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="hosein2" class="tabcontent">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="">test2</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="">test2</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/product/cache_cats_1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="hosein3" class="tabcontent">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="">test3</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="">test3</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/product/cache_cats_1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="hosein4" class="tabcontent">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="">test4</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="">test4</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/product/cache_cats_1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="hosein5" class="tabcontent">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="">test5</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="">test5</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/product/cache_cats_1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                <form action="#" method="get" id="searchForm" class="input-group">
                    
                            <div class="input-group-btn search-panel ">
                                <select name="search_param" id="search_param" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <option value="all">&nbsp;&nbsp;همه دسته ها&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="username">Username</option>
                                    <option value="email">Email</option>
                                    <option value="studentcode">Student Code</option>
                                </select>
                            </div>
                            <input type="text" class="form-control" name="x" placeholder="نام کالا یا برند خود را وارد نمایید">
                            <span class="input-group-btn ">
                                <button class="btn btn-default btn-search" type="submit">
                                <span class="flaticon flaticon-magnifying-glass"></span>
                                </button>
                            </span>
                        </form><!-- end form -->



                    
                </div>
                <ul class="navbar-brand">
                    <li><a type="button" class="btn"><span class="color-gold flaticon flaticon-avatar"></span> ثبت نام </a></li>
                    <li><a type="button" class="btn"><span class="color-gold flaticon flaticon-login"></span> ورود</a></li>
                    <li><a type="button" class="btn">سبد (۹)<span class="color-gold flaticon flaticon-shopping-cart"></span> </a></li>
                    <li><a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a></li>

                </ul>
            </nav>
        </div>
    </div>
</header>