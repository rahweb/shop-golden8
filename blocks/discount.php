<div class="discount">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <div class="padding-left">
                    <div class="h-s">
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><span></span>discount<span class="span"></span></h5>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <div class="sid-left-a text-center height">
                                <br><br>
                                <h5>Up to 30% off discounts</h5>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <a class="ad-btn" href="#"><span class="flaticon flaticon-back color-gold"></span>  All the wonders</a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <div class="row" id="ads">
                                <!-- Category Card -->
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="padding-right">
                    <div class="h-s">
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><span></span>discount<span class="span"></span></h5>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <div class="sid-left-b text-center height">
                                <br><br>
                                <h5>Up to 30% off discounts</h5>
                                <br>
                                <br>
                                <h4 class="font">Types of table and stand</h4>
                                <br>
                                <br>
                                <a class="ad-btn" href="#"><span class="flaticon flaticon-back color-gold"></span>  All the wonders</a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <div class="row" id="ads">
                                <!-- Category Card -->
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <div class="padding-left">
                    <div class="h-s">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                        <h5><span></span>discount<span class="span"></span></h5>
                        </div>
                    </div>
                    <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <div class="sid-left-c text-center height">
                            <br><br>
                            <h5>Up to 30% off discounts</h5>
                            <br>
                            <br>
                            <h4 class="font">Types of table and stand</h4>
                            <br>
                            <br>
                            <a class="ad-btn" href="#"><span class="flaticon flaticon-back color-gold"></span>  All the wonders</a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <div class="row" id="ads">
                            <!-- Category Card -->
                            <div class="col-md-6">
                                <div class="card rounded">
                                    <div class="card-image">
                                        <span class="card-notify-year">40%</span>
                                        <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="ad-title m-auto">
                                            <h5>Honda Accord LX</h5>
                                        </div>
                                        <span class="color-gold">24,000,000 $</span>
                                    </div>
                                    <div class="overlay">
                                        <div class="text-t">
                                        <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                        <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card rounded">
                                    <div class="card-image">
                                        <span class="card-notify-year">40%</span>
                                        <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="ad-title m-auto">
                                            <h5>Honda Accord LX</h5>
                                        </div>
                                        <span class="color-gold">24,000,000 $</span>
                                    </div>
                                    <div class="overlay">
                                        <div class="text-t">
                                        <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                        <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="padding-right">
                    <div class="h-s">
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><span></span>discount<span class="span"></span></h5>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <div class="sid-left-d text-center height">
                                <br><br>
                                <h5>Up to 30% off discounts</h5>
                                <br>
                                <br>
                                <h4 class="font">Types of table and stand</h4>
                                <br>
                                <br>
                                <a class="ad-btn" href="#"><span class="flaticon flaticon-back color-gold"></span>  All the wonders</a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <div class="row" id="ads">
                                <!-- Category Card -->
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                            <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card rounded">
                                        <div class="card-image">
                                            <span class="card-notify-year">40%</span>
                                            <img class="img-fluid image" src="images/product/images_product_12.jpg"  alt="Alternate Text">
                                        </div>
                                        <div class="card-body text-center">
                                            <div class="ad-title m-auto">
                                                <h5>Honda Accord LX</h5>
                                            </div>
                                            <span class="color-gold">24,000,000 $</span>
                                        </div>
                                        <div class="overlay">
                                            <div class="text-t">
                                                <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span></a>
                                                <a type="button" class="btn"><span class="color-gold flaticon flaticon-like"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>