<div class="services">
    <br>
    <div class="container">
        <div class="row bac">
            <div class="col-md col-lg col-xs-6 col-sm-6">
                <div class="row">
                    <div class="col-md-3 right">
                        <span class="flaticon flaticon-champion color-gold"></span>
                    </div>
                    <div class="col-md-9 left">
                        <h6>Guarantee the originality</h6>
                        <p>Original and standard goods</p>
                    </div>
                </div>
            </div>
            <div class="col-md col-lg col-xs-6 col-sm-6">
                <div class="row">
                    <div class="col-md-3 right">
                        <span class="flaticon flaticon-24-hours color-gold"></span>
                    </div>
                    <div class="col-md-9 left">
                        <h6>24-hour support</h6>
                        <p>We respond to you at night</p>
                    </div>
                </div>
            </div>
            <div class="col-md col-lg col-xs-6 col-sm-6">
                <div class="row">
                    <div class="col-md-3 right">
                        <span class="flaticon flaticon-credit-card color-gold"></span>
                    </div>
                    <div class="col-md-9 left">
                        <h6>Payment at the place</h6>
                        <p>For those unable to pay online</p>
                    </div>
                </div>
            </div>
            <div class="col-md col-lg col-xs-6 col-sm-6">
                <div class="row">
                    <div class="col-md-3 right">
                        <span class="flaticon flaticon-logistics-delivery-truck-in-movement color-gold"></span>
                    </div>
                    <div class="col-md-9 left">
                        <h6>Instant delivery</h6>
                        <p>Up to 48 hours delivery in all cities</p>
                    </div>
                </div>
            </div>
            <div class="col-md col-lg col-xs-6 col-sm-6">
                <div class="row">
                    <div class="col-md-3 right">
                        <span class="flaticon flaticon-champion color-gold"></span>
                    </div>
                    <div class="col-md-9 left">
                        <h6>7 days warranty return of</h6>
                        <p>Original and standard goods</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>