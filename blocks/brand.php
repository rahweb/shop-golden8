<div class="brand">
    <div class="h-s">
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5><span></span>Home Appliances <span class="span"></span></h5>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div class="container">
        <div class="row blog">
            <div class="col-md-12">
                <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/0ede8cb9c9.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/59f1ae2cc4.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/5b2b26e353.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/81dbbf60c8.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/c61b71894a.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.item-->
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/0ede8cb9c9.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/59f1ae2cc4.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/5b2b26e353.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/81dbbf60c8.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/c61b71894a.jpg" alt="Image">
                                    </a>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img src="images/brand/dc4174b634.jpg" alt="Image">
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.item-->
                    </div>
                    <!--.carousel-inner-->
                </div>
                <!--.Carousel-->
            </div>
        </div>
    </div>
</div>