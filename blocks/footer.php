<div class="footer">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <h5 class="color-gold">Contacts</h5>
                <br>
                <ul>
                    <li>Sales email : <a href=""> INFO@GOLDEN8.IR</a></li>
                    <li>Support email : <a href=""> INFO@GOLDEN8.IR</a></li>
                    <li>Sales phone : <span> 02122334455</span></li>
                    <li>Support phone : <span> 02122334455</span></li>
                    <li>the address of Central office : <span> Tehran-Km5-Central Karaj Special Road</span></li>
                </ul>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 text-center center">
                <p>Enter your email for the latest discounts and products</p>
                <input type="text" class="color-code" id="color" value="" placeholder="Enter email">
                <div class="row sb">
                    <div class="col-md col-lg col-sm-4 col-xs-4">
                        <br>
                        <a href="">
                            <img src="images/footer/footer-1.png" alt="">
                        </a>
                    </div>
                    <div class="col-md col-lg col-sm-4 col-xs-4">
                        <br>
                        <a href="">
                            <img src="images/footer/footer-2.png" alt="">
                        </a>
                    </div>
                    <div class="col-md col-lg col-sm-4 col-xs-4">
                        <br>
                        <a href="">
                            <img src="images/footer/footer-3.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md">
                        <h5 class="color-gold">Useful links</h5>
                        <br>
                        <ul>
                            <li><a href="#">Supplier panel</a></li>
                            <li><a href="#">Panel of deputies</a></li>
                            <li><a href="#">Central store</a></li>
                            <li><a href="#">Register complaints</a></li>
                            <li><a href="#">Terms and Conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-md">
                        <h5 class="color-gold">Products</h5>
                        <br>
                        <ul>
                            <li><a href="#">best sellers</a></li>
                            <li><a href="#">Discounts</a></li>
                            <li><a href="#">Winter Festival</a></li>
                            <li><a href="#">Most Viewed</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row" style="padding: 0% 2%;">
            <p class="color-gold">Golden8 Online Store,Check, select and buy online</p>
            <p class="line">
                In the present era, we see a lot of changes in different parts of the human life style, which undoubtedly are one of the most important ones. Specifically, shoppers prefer to go to stores where they can meet all their needs, instead of spending time and energy on visiting multiple stores, has led to the growing growth of shopping and supermarkets in the world. Therefore, we decided to build a first home-made home-office high-quality home theater Golden8, with an area of ​​3000 square meters, with great investment in a 10,000-square-meter area, to provide the comfort and convenience of our compatriots for a safe and easy purchase. And we need a decent response to this growing need of our people.
            </p>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <p>
                The use of the Golden 8 Web Store materials is for non-commercial purposes only, with reference to the source. All rights reserved by Noavarane Fan Aveza (Golden8 Online Store Online)
            </p>
        </div>
    </div>
</div>