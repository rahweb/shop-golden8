<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/header.php");?>
    <?php include("blocks/slider.php");?>
    <?php include("blocks/services.php");?>
    <?php include("blocks/product.php");?>
    <?php include("blocks/baner.php");?>
    <?php include("blocks/discount.php");?>
    <?php include("blocks/baner2.php");?>
    <?php include("blocks/product-t.php");?>
    <?php include("blocks/brand.php");?>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>