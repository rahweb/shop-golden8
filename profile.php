<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/header.php");?>

    <!-- start profile -->
    <div class="row profile-user" style="margin-top:0px;">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3 side">
                    <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="images/avatar.png" class="img-responsive" alt="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                اعظم جعفری
                            </div>
                           
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">
                            <button type="button" class="btn btn-success btn-sm btn-golden ">تغییر رمز</button>
                            <button type="button" class="btn btn-danger btn-sm btn-black">ارسال تیکت</button>
                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="#">
                                    <span class="color-gold flaticon flaticon-avatar"></span>
                                    پروفایل </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <span class="color-gold flaticon flaticon-shopping-cart"></span>
                                    سفارشات </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                    <span class="flaticon flaticon-24-hours color-gold"></span>
                                    درخواست های مرجوعی </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <span  class="color-gold flaticon flaticon-list-with-dots"></span>
                                    کد تخفیف </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                    <span  class="color-gold flaticon flaticon-list-with-dots"></span>
                                   آدرس ها </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <span  class="color-gold flaticon flaticon-list-with-dots"></span>
                                   تیکت ها</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9 body-profile">
                    <div class="">
                        <div class="h-s">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5><span></span>اطلاعات شخصی<span class="span"></span></h5>
                                    <div class="row">
                                   </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>

                          <form action="" method="">
                            <div class="row profile-content">
                                        <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">نام </label>
                                                    <div class="col-md-6">
                                                        <input type="text" id="email_address" class="form-control" name="email-address" required="" autofocus="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password" class="col-md-4 col-form-label text-md-right">نام خانوادگی</label>
                                                    <div class="col-md-6">
                                                        <input type="password" id="password" class="form-control" name="password" required="">
                                                    </div>
                                                </div>

                                                 <div class="form-group row">
                                                    <label for="password" class="col-md-4 col-form-label text-md-right">کد ملی</label>
                                                    <div class="col-md-6">
                                                        <input type="password" id="password" class="form-control" name="password" required="">
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group row">
                                                <label for="email_address" class="col-md-4 col-form-label text-md-right">شماره موبایل</label>
                                                <div class="col-md-8">
                                                    <input type="text" id="email_address" class="form-control" name="email-address" required="" autofocus="">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">شماره کارت</label>
                                                <div class="col-md-8">
                                                    <input type="password" id="password" class="form-control" name="password" required="">
                                                </div>

                                                
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">ایمیل</label>
                                                <div class="col-md-8">
                                                    <input type="password" id="password" class="form-control" name="password" required="">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-8">
                                                    <input type="submit" id="password" class="form-control btn btn-golden"  value="ذخیره اطلاعات">
                                                </div>
                                            </div>




                                        </div>
                                </div>       
                            </form> 




                    </div>
                </div>
            </div>
        </div>
   </div>


 
    <!-- end profile -->
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>