<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/header.php");?>
    <div class="product-d">
        <div class="br-h">
            <br>
            <div class="container">
                <ul>
                    <li class="home"><a href="">HOME</a></li>
                    <li><span class="flaticon flaticon-next"></span></li>
                    <li><a href="">CATEGORY</a></li>
                    <li><span class="flaticon flaticon-next"></span></li>
                    <li>REFRIGERATOR</li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <br>
                    <div class="preview col">
                        <div class="api-controls">
                            <button class="cfg-btn">
                                <span style="font-size:20px" class="flaticon flaticon-like"></span>
                            </button>
                            <button class="cfg-btn" onclick="MagicZoom.prev('Zoom-1')">
                                <span style="font-size: 20px;" class="flaticon flaticon-back"></span>
                            </button>
                            <button class="cfg-btn" onclick="MagicZoom.expand('Zoom-1')" title="Plus version only." >
                                <span style="font-size: 20px;" class="flaticon flaticon-magnifying-glass"></span>
                            </button>
                            <button class="cfg-btn" onclick="MagicZoom.next('Zoom-1')">
                                <span style="font-size: 20px;" class="flaticon flaticon-next"></span>
                            </button>
                            <button class="cfg-btn">
                                <span style="font-size:20px" class="flaticon flaticon-share"></span>
                            </button>
                        </div>
                        <div class="app-figure" id="zoom-fig">
                            <a id="Zoom-1" class="MagicZoom" title="Show your product in stunning detail with Magic Zoom Plus."
                                href="images/product/sampl1.jpg" >
                                <img src="images/product/sampl1.jpg?scale.height=400" alt=""/>
                            </a>
                            <div class="selectors">
                                <a data-zoom-id="Zoom-1" href="images/product/sampl1.jpg"
                                    data-image="images/product/sampl1.jpg?scale.height=400">
                                    <img srcset="images/product/sampl1.jpg?scale.width=112 4x" src="images/product/sampl1.jpg?scale.width=56"/>
                                </a>
                                <a data-zoom-id="Zoom-1" href="images/product/sampl3.jpg"
                                    data-image="images/product/sampl3.jpg?scale.height=400">
                                    <img srcset="images/product/sampl3.jpg?scale.width=112 2x" src="images/product/sampl3.jpg?scale.width=56"/>
                                </a>
                                <a data-zoom-id="Zoom-1" href="images/product/sample2.jpg"
                                    data-image="images/product/sample2.jpg?scale.height=400">
                                    <img srcset="images/product/sample2.jpg?scale.width=112 2x" src="images/product/sample2.jpg?scale.width=56"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                    <br>
                    <div class="title">
                        <div class="tex">
                            <h3>Samsung TV 55M6970</h3>
                            <p>Model: Samsung 55M6970 Smart LED TV 55 Inch</p>
                        </div>
                        <div class="svg">
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                        </div>
                        <hr>
                        <ul>
                            <li class="hom"><p>Brand : Samsung</p></li>
                            <li><p>category :<a href="">Television</a></p></li>
                        </ul>
                        <br>
                    </div>
                    <div class="specifications">
                        <ul>
                            <li class="hom">
                                <img src="images/icons8-guarantee-480.png" alt=""> 
                                Samservice 18-month warranty
                            </li>
                            <li class="hom">
                                <img src="images/icons8-online-store-480.png" alt=""> 
                                Dealer: ASICO Broadcast
                            </li>
                            <li class="hom">
                                <img src="images/icons8-in-transit-480.png" alt="">  
                                Send from 1 next day
                            </li>
                            <li class="color hom">Price : <span>6,197,000 tomans</span></li>
                            <li class="hom">
                                <a type="button" class="btn"><span class="color-gold flaticon flaticon-shopping-cart"></span> Add to cart </a>
                            </li>
                            <li class="hom">
                                <p>Are you satisfied with the price? <a href="">Yes</a> | <a href="">No</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/services.php");?>
        <div class="h-s">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5><span></span>Related products <span class="span"></span></h5>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="container">
            <div class="row">
                <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                    <div class="MultiCarousel-inner">
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                        <a href="" class="item">
                            <div class="pad15">
                                <img src="images/product/sampl1.jpg" alt="">
                                <p>Samsung 55M6970 Smart LED TV 55 Inch</p>
                                <span>10,000,000</span>
                            </div>
                        </a>
                    </div>
                    <button class="btn btn-primary leftLst"><span class="flaticon flaticon-back"></span></button>
                    <button class="btn btn-primary rightLst"><span class="flaticon flaticon-next"></span></button>
                </div>
            </div>
        </div>
        <div class="h-s">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5><span></span>Related products <span class="span"></span></h5>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">
                        <svg style="width:7%" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="glasses" class="svg-inline--fa fa-glasses fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor" d="M574.1 280.37L528.75 98.66c-5.91-23.7-21.59-44.05-43-55.81-21.44-11.73-46.97-14.11-70.19-6.33l-15.25 5.08c-8.39 2.79-12.92 11.86-10.12 20.24l5.06 15.18c2.79 8.38 11.85 12.91 20.23 10.12l13.18-4.39c10.87-3.62 23-3.57 33.16 1.73 10.29 5.37 17.57 14.56 20.37 25.82l38.46 153.82c-22.19-6.81-49.79-12.46-81.2-12.46-34.77 0-73.98 7.02-114.85 26.74h-73.18c-40.87-19.74-80.08-26.75-114.86-26.75-31.42 0-59.02 5.65-81.21 12.46l38.46-153.83c2.79-11.25 10.09-20.45 20.38-25.81 10.16-5.3 22.28-5.35 33.15-1.73l13.17 4.39c8.38 2.79 17.44-1.74 20.23-10.12l5.06-15.18c2.8-8.38-1.73-17.45-10.12-20.24l-15.25-5.08c-23.22-7.78-48.75-5.41-70.19 6.33-21.41 11.77-37.09 32.11-43 55.8L1.9 280.37A64.218 64.218 0 0 0 0 295.86v70.25C0 429.01 51.58 480 115.2 480h37.12c60.28 0 110.37-45.94 114.88-105.37l2.93-38.63h35.75l2.93 38.63C313.31 434.06 363.4 480 423.68 480h37.12c63.62 0 115.2-50.99 115.2-113.88v-70.25c0-5.23-.64-10.43-1.9-15.5zm-370.72 89.42c-1.97 25.91-24.4 46.21-51.06 46.21H115.2C86.97 416 64 393.62 64 366.11v-37.54c18.12-6.49 43.42-12.92 72.58-12.92 23.86 0 47.26 4.33 69.93 12.92l-3.13 41.22zM512 366.12c0 27.51-22.97 49.88-51.2 49.88h-37.12c-26.67 0-49.1-20.3-51.06-46.21l-3.13-41.22c22.67-8.59 46.08-12.92 69.95-12.92 29.12 0 54.43 6.44 72.55 12.93v37.54z"></path>
                        </svg> 
                        REVIEW
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">
                        <svg style="width:6%" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="clipboard-list" class="svg-inline--fa fa-clipboard-list fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                            <path fill="currentColor" d="M336 64h-80c0-35.3-28.7-64-64-64s-64 28.7-64 64H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM96 424c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm0-96c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm0-96c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm96-192c13.3 0 24 10.7 24 24s-10.7 24-24 24-24-10.7-24-24 10.7-24 24-24zm128 368c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-96c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-96c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16z"></path>
                        </svg> 
                        SPECIFICATIONS
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu2">
                        <svg style="width:8%" aria-hidden="true" focusable="false" data-prefix="far" data-icon="comments" class="svg-inline--fa fa-comments fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor" d="M532 386.2c27.5-27.1 44-61.1 44-98.2 0-80-76.5-146.1-176.2-157.9C368.3 72.5 294.3 32 208 32 93.1 32 0 103.6 0 192c0 37 16.5 71 44 98.2-15.3 30.7-37.3 54.5-37.7 54.9-6.3 6.7-8.1 16.5-4.4 25 3.6 8.5 12 14 21.2 14 53.5 0 96.7-20.2 125.2-38.8 9.2 2.1 18.7 3.7 28.4 4.9C208.1 407.6 281.8 448 368 448c20.8 0 40.8-2.4 59.8-6.8C456.3 459.7 499.4 480 553 480c9.2 0 17.5-5.5 21.2-14 3.6-8.5 1.9-18.3-4.4-25-.4-.3-22.5-24.1-37.8-54.8zm-392.8-92.3L122.1 305c-14.1 9.1-28.5 16.3-43.1 21.4 2.7-4.7 5.4-9.7 8-14.8l15.5-31.1L77.7 256C64.2 242.6 48 220.7 48 192c0-60.7 73.3-112 160-112s160 51.3 160 112-73.3 112-160 112c-16.5 0-33-1.9-49-5.6l-19.8-4.5zM498.3 352l-24.7 24.4 15.5 31.1c2.6 5.1 5.3 10.1 8 14.8-14.6-5.1-29-12.3-43.1-21.4l-17.1-11.1-19.9 4.6c-16 3.7-32.5 5.6-49 5.6-54 0-102.2-20.1-131.3-49.7C338 339.5 416 272.9 416 192c0-3.4-.4-6.7-.7-10C479.7 196.5 528 238.8 528 288c0 28.7-16.2 50.6-29.7 64z"></path>
                        </svg> 
                        USER COMMENTS
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu3">
                        <svg style="width:8%" aria-hidden="true" focusable="false" data-prefix="far" data-icon="question-circle" class="svg-inline--fa fa-question-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z"></path>
                        </svg>  
                        QUESTION AND ANSWER
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="home" class="tab-pane active"><br>
                    <h3>REVIEW</h3>
                    <h5>Samsung Smart LED TV 82NU8900 82-inch model</h5>
                    <br>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <img src="images/product/writer.png" alt="">
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                            <p>
                                "82NU8900" is a flat-screen TV from Samsung, which uses Smartphone OS. The elegant, stunning design and 4K image quality are one of the most important features of the TV; of course, HDR support is also a feature of the TV. Its design is different and attractive and can attract the attention of each audience. Its 82-inch dimensions are large enough for large halls, and viewers can easily look at wide screen angles.
                            </p>
                            <p>
                                The quality of the image is 4K, and with a high quality video display, you can see the smallest details of the image; however, to get the quality of the image, this TV and all 4K TVs should provide high-quality, high-quality file. The HDR capability on this TV also allows you to get more color and brightness. The sound produced by this TV also has a fairly large power. With its built-in subwoofer, it can generate 40 watts of sound, which will meet the needs of most users, and will not require a separate speaker. 
                                The support of this model for advanced and advanced audio systems delivers the pleasure of watching the movie. The 82NU8900 comes with 3 USB ports and 4 HDMI ports. You can connect your flash drive or hard disk to the TV via the USB port and use the information in it. Also, with HDMI ports, you can connect other audio and video devices that are equipped with this input to the TV to connect to the TV. Playback content, Full HD, or 4K images. To connect this TV to older CDs or DVDs, 
                                there's also a composite and component interface built into the body so the user can uninterruptedly match his TV to all audio and video devices. The product is also equipped with Smartphone Interface, which allows users to easily customize their settings on the TV and enjoy the same features as searching on the Internet and using various applications, as well as DLNA capabilities, you can play the image from the mobile phone. See on your TV. This model is equipped with an internal digital receiver that allows you to receive digital audio channels. 
                                Users can, according to their taste, put this TV on a desk or connect it to a wall with a separate wall base and enjoy more space.
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <div class="side">
                                <div>The quality and transparency of the image</div>
                            </div>
                            <div class="middle">
                                <div class="bar-container">
                                    <div class="bar-5"></div>
                                </div>
                            </div>
                            <div class="side right">
                                <div>Excellent</div>
                            </div>
                            <div class="side">
                                <div>Features and capabilities</div>
                            </div>
                            <div class="middle">
                                <div class="bar-container">
                                    <div class="bar-4"></div>
                                </div>
                            </div>
                            <div class="side right">
                                <div>Good</div>
                            </div>
                            <div class="side">
                                <div>Ease of use</div>
                            </div>
                            <div class="middle">
                                <div class="bar-container">
                                    <div class="bar-3"></div>
                                </div>
                            </div>
                            <div class="side right">
                                <div>medium</div>
                            </div>
                            <div class="side">
                                <div>Value versus price</div>
                            </div>
                            <div class="middle">
                                <div class="bar-container">
                                    <div class="bar-2"></div>
                                </div>
                            </div>
                            <div class="side right">
                                <div>Normal</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <h5 class="color-gold">Strengths</h5>
                            <ul style="padding:0% 3%">
                                <li>4k image quality</li>
                                <li>Supports hdr</li>
                                <li>Smart Hub Smart Interface</li>
                                <li>Various communication ports</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade"><br>
                    <h3>Technical Specifications</h3>
                    <h5>Samsung Smart LED TV 82NU8900 82-inch model</h5>
                    <br>
                    <div class="near_by_hotel_wrapper">
                        <div class="near_by_hotel_container">
                            <table class="table no-border custom_table dataTable no-footer dtr-inline">
                                <colgroup>
                                    <col width="30%">
                                    <col width="70%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="color-gold"><span class="flaticon flaticon-next"></span> screen</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Page size</td>
                                        <td class="text-left">82 inches</td>
                                    </tr>
                                    <tr>
                                        <td>Page technology</td>
                                        <td class="text-left">Led</td>
                                    </tr>
                                    <tr>
                                        <td>Page type</td>
                                        <td class="text-left">Flat</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="near_by_hotel_wrapper">
                        <div class="near_by_hotel_container">
                            <table class="table no-border custom_table dataTable no-footer dtr-inline">
                                <colgroup>
                                    <col width="30%">
                                    <col width="70%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="color-gold"><span class="flaticon flaticon-next"></span> the image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Page size</td>
                                        <td class="text-left">82 inches</td>
                                    </tr>
                                    <tr>
                                        <td>Page technology</td>
                                        <td class="text-left">Led</td>
                                    </tr>
                                    <tr>
                                        <td>Page type</td>
                                        <td class="text-left">Flat</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade"><br>
                    <h3>Users Points to:</h3>
                    <h5>Samsung Smart LED TV 82NU8900 82-inch model</h5>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                    <p class="text-secondary text-center">15 Minutes Ago</p>
                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <a class="float-left" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Maniruzzaman Akash</strong></a>
                                        <div class="svg" style="margin-top: -2%;">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="checked svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                        </div>
                                    </p>
                                    <div class="clearfix"></div>
                                    <br>
                                    <p>
                                        Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    </p>
                                    <p>
                                        <a class="float-right btn btn-outline-primary ml-2"> 
                                            <svg style="width:30%;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="reply-all" class="svg-inline--fa fa-reply-all fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M136.309 189.836L312.313 37.851C327.72 24.546 352 35.348 352 56.015v82.763c129.182 10.231 224 52.212 224 183.548 0 61.441-39.582 122.309-83.333 154.132-13.653 9.931-33.111-2.533-28.077-18.631 38.512-123.162-3.922-169.482-112.59-182.015v84.175c0 20.701-24.3 31.453-39.687 18.164L136.309 226.164c-11.071-9.561-11.086-26.753 0-36.328zm-128 36.328L184.313 378.15C199.7 391.439 224 380.687 224 359.986v-15.818l-108.606-93.785A55.96 55.96 0 0 1 96 207.998a55.953 55.953 0 0 1 19.393-42.38L224 71.832V56.015c0-20.667-24.28-31.469-39.687-18.164L8.309 189.836c-11.086 9.575-11.071 26.767 0 36.328z"></path></svg>
                                            Reply
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="card card-inner">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                            <p class="text-secondary text-center">15 Minutes Ago</p>
                                        </div>
                                        <div class="col-md-10">
                                            <p>
                                                <a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>admin</strong></a></p>
                                            <p>
                                                Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                            </p>
                                            <p>
                                                <a class="float-right btn btn-outline-primary ml-2"> 
                                                    <svg style="width:30%;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="reply-all" class="svg-inline--fa fa-reply-all fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M136.309 189.836L312.313 37.851C327.72 24.546 352 35.348 352 56.015v82.763c129.182 10.231 224 52.212 224 183.548 0 61.441-39.582 122.309-83.333 154.132-13.653 9.931-33.111-2.533-28.077-18.631 38.512-123.162-3.922-169.482-112.59-182.015v84.175c0 20.701-24.3 31.453-39.687 18.164L136.309 226.164c-11.071-9.561-11.086-26.753 0-36.328zm-128 36.328L184.313 378.15C199.7 391.439 224 380.687 224 359.986v-15.818l-108.606-93.785A55.96 55.96 0 0 1 96 207.998a55.953 55.953 0 0 1 19.393-42.38L224 71.832V56.015c0-20.667-24.28-31.469-39.687-18.164L8.309 189.836c-11.086 9.575-11.071 26.767 0 36.328z"></path></svg>
                                                    Reply
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu3" class="tab-pane fade"><br>
                    <h3>question and answer</h3>
                    <h5>Samsung Smart LED TV 82NU8900 82-inch model</h5>
                    <br>
                    <div class="row">
                        <div class="widget-area no-padding blank">
                            <p>Post your question about the product</p>
                            <div class="status-upload">
                                <form>
                                    <textarea placeholder="What are you doing right now?" ></textarea>
                                    <button style="width:20%" type="submit" class="btn btn-black">
                                        <svg style="width:9%;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="reply-all" class="svg-inline--fa fa-reply-all fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M136.309 189.836L312.313 37.851C327.72 24.546 352 35.348 352 56.015v82.763c129.182 10.231 224 52.212 224 183.548 0 61.441-39.582 122.309-83.333 154.132-13.653 9.931-33.111-2.533-28.077-18.631 38.512-123.162-3.922-169.482-112.59-182.015v84.175c0 20.701-24.3 31.453-39.687 18.164L136.309 226.164c-11.071-9.561-11.086-26.753 0-36.328zm-128 36.328L184.313 378.15C199.7 391.439 224 380.687 224 359.986v-15.818l-108.606-93.785A55.96 55.96 0 0 1 96 207.998a55.953 55.953 0 0 1 19.393-42.38L224 71.832V56.015c0-20.667-24.28-31.469-39.687-18.164L8.309 189.836c-11.086 9.575-11.071 26.767 0 36.328z"></path></svg>
                                        Submit a question
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>