<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/header.php");?>
    <div class="product-l">
        <div class="br-h">
            <br>
            <div class="container">
                <ul>
                    <li class="home"><a href="">HOME</a></li>
                    <li><span class="flaticon flaticon-next"></span></li>
                    <li><a>PRODUCT LIST</a></li>
                </ul>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group filter-wrap">
                        <article class="list-group-item">
                            <header class="filter-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse1">
                                    <h6 class="title">Categorize the results <span class="flaticon flaticon-down-arrow"></span></h6>
                                </a>
                            </header>
                            <hr>
                            <div class="filter-content collapse show" id="collapse1">
                                <ul>
                                    <li><a href="">Home and kitchen <span class="flaticon flaticon-next"></span></a></li>
                                    <li><a href="">Home and kitchen <span class="flaticon flaticon-next"></span></a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="list-group-item">
                            <header class="filter-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" class="">
                                    <h6 class="title">Search results <span class="flaticon flaticon-down-arrow"></span></h6>
                                </a>
                            </header>
                            <hr>
                            <div class="filter-content collapse" id="collapse2" style="">			
                                <form class="py-2">
                                    <div class="input-group">
                                    <input style="font-size: 0.6vw;" type="text" class="form-control" placeholder="Write the name of the product or brand...">
                                    </div>
                                </form>
                            </div>
                        </article>
                        <article class="list-group-item">
                            <header class="filter-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" class="">
                                    <h6 class="title">brand <span class="flaticon flaticon-down-arrow"></span></h6>
                                </a>
                            </header>
                            <hr>
                            <div class="filter-content collapse" id="collapse3" style="">			
                                <form class="py-2">
                                    <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    </div>
                                </form>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                SAMSUNG
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                LG
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                SONY
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                MARSHAL
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </article>
                        <article class="list-group-item">
                            <header class="filter-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" class="">
                                    <h6 class="title">Colors <span class="flaticon flaticon-down-arrow"></span></h6>
                                </a>
                            </header>
                            <hr>
                            <div class="filter-content collapse" id="collapse4" style="">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                Black
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                silver
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                Pencil tip
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </article>
                        <article class="list-group-item">
                            <header class="filter-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" class="">
                                    <h6 class="title">Kind of <span class="flaticon flaticon-down-arrow"></span></h6>
                                </a>
                            </header>
                            <hr>
                            <div class="filter-content collapse" id="collapse5" style="">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                55 inches and larger
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                From 42 to 48 inches
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                40 inches and smaller
                                            </label>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">
                                                49 to 52 inches
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <br>
                    <div style="background: #fff;padding: 2% 0% 0% 5%;" class="row">
                        <div class="col-md-9" style="padding: 2% 0% 0% 0%;">
                            <p>Only available goods</p>
                        </div>
                        <div class="col-md-3">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <br>
                    <div style="background: #fff;padding: 2% 0% 0% 5%;" class="row">
                        <div class="col-md-9" style="padding: 2% 0% 0% 0%;">
                            <p>Just ready to send goods</p>
                        </div>
                        <div class="col-md-3">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" style="padding: 0% 0% 0% 1%;">
                    <div class="row  my-3">
                        <div class="col-lg-11">
                            <ul class="c-listing__sort js-sort-options" data-label="order by :">
                                <li>
                                    <a href="javascript:" data-sort="22" class="is-active">Most Popular</a>
                                </li>
                                <li>
                                    <a href="javascript:" data-sort="1" >the newest</a>
                                </li>
                                <li>
                                    <a href="javascript:" data-sort="7" >Bestselling</a>
                                </li>
                                <li>
                                    <a href="javascript:" data-sort="20" >cheapest</a>
                                </li>
                                <li>
                                    <a href="javascript:" data-sort="21" >the most expensive</a>
                                </li>
                                <li>
                                    <a href="javascript:" data-sort="25" >The fastest post</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-1">
                            <div class="pull-right">
                                <div class="btn-group" style="margin-top: 10%;">
                                    <button style="width:100%;padding:5%" class="btn btn-golden" id="list">
                                        <svg style="width:100%" aria-hidden="true" data-prefix="fas" data-icon="th-list" class="svg-inline--fa fa-th-list fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M149.333 216v80c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24v-80c0-13.255 10.745-24 24-24h101.333c13.255 0 24 10.745 24 24zM0 376v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H24c-13.255 0-24 10.745-24 24zM125.333 32H24C10.745 32 0 42.745 0 56v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24V56c0-13.255-10.745-24-24-24zm80 448H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24zm-24-424v80c0 13.255 10.745 24 24 24H488c13.255 0 24-10.745 24-24V56c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24zm24 264H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24z"></path></svg>
                                    </button>
                                    <button style="width:100%;padding:5%" class="btn btn-golden" id="grid">
                                        <svg style="width:100%" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="th" class="svg-inline--fa fa-th fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M149.333 56v80c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24V56c0-13.255 10.745-24 24-24h101.333c13.255 0 24 10.745 24 24zm181.334 240v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.256 0 24.001-10.745 24.001-24zm32-240v80c0 13.255 10.745 24 24 24H488c13.255 0 24-10.745 24-24V56c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24zm-32 80V56c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.256 0 24.001-10.745 24.001-24zm-205.334 56H24c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24zM0 376v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H24c-13.255 0-24 10.745-24 24zm386.667-56H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24zm0 160H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24zM181.333 376v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24z"></path></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div id="products" class="row view-group">
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="images/product/sampl1.jpg" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        Product title</h4>
                                    <p class="group inner list-group-item-text">
                                        Samsung LED TV 43N5980 43 inches size
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p class="lead">10,233,000 toman</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <a class="btn btn-black" href="http://www.jquery2dotnet.com">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>