<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/header.php");?>

    <!-- start profile -->
    <div class="row profile-user" style="margin-top:0px;">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3 side">
                    <?php include("plagin/side-panel.php");?>
                </div>
                <div class="col-md-9 body-profile">
                    <div class="">
                        <div class="h-s">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5><span></span>آدرس ها<span class="span"></span></h5>
                                    <div class="row">
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                        <form action="" method="">
                            <div class="row profile-content">
                                <h4>
                                    <a href="">
                                        <span class="color-gold flaticon flaticon-placeholder"></span>  
                                        افزودن آدرس
                                    </a>
                                </h4>
                                <a href="#" class="btn location text-center">
                                    <span class="color-gold flaticon flaticon-placeholder"></span> 
                                    <br>
                                    افزودن آدرس جدید
                                </a>
                                <div class="location-n">
                                    <h4>اعظم جعفری</h4>
                                    <p> استان تهران، ‌شهر تهران، محله 17 شهریور، تهران تهران</p>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md l">

                                        </div>
                                        <div class="col-md o">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end profile -->
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>